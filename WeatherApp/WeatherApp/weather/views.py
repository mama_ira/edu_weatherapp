import requests
from django.shortcuts import render


def index(request):
    app_id = '80e99e13c022dc6385a28acc2b92e284'
    city = 'London'
    url = f'https://api.openweathermap.org/data/2.5/weather?q={city}&appid={app_id}'

    response = requests.get(url)
    data = response.json()
    print(data)

   
    
    return render(request, 'weather/index.html', {"weather": data})


def about(request):
	return render(request, 'weather/about.html')

